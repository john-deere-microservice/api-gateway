package com.classpath.apigateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.UUID;

@Component
@Slf4j
public class TrackingFilter implements GlobalFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info(" Adding the correlation id in the request header :: ");
        boolean isCorrelationIdPresent = exchange.getRequest().getHeaders().containsKey("CORRELATION_ID");
        if(!isCorrelationIdPresent){
            String correlationId = UUID.randomUUID().toString();
            log.info("Correlation Id generated :: {}", correlationId);
            exchange.getRequest().mutate().header("CORRELATION_ID", correlationId);
        }
        return chain.filter(exchange);
    }
}